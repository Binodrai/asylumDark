function enableStyle() {
    chrome.extension.sendRequest('show_page_action');
    document.querySelector('body').classList.add('darkTh');
}

document.addEventListener('DOMContentLoaded', function() {
    enableStyle();
});

chrome.runtime.onMessage.addListener(function(request) {
    if (request === 'enable') {
        enableStyle();
    } else if (request === 'disable') {
        document.querySelector('body').classList.remove('darkTh');
    }
});