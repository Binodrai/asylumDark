module.exports = function(grunt) {
    require('jit-grunt')(grunt);
    grunt.registerTask('default', ['build']);
    grunt.registerTask('dev', ['build', 'concurrent']);

    grunt.registerTask('build', ['clean', 'copy', 'styles', 'scripts']);

    grunt.registerTask('styles', ['sass']);
    grunt.registerTask('scripts', ['jshint']);

    // 'shell:bower',

    grunt.initConfig({
        // shell: {
        //     // install bower packages
        //     bower: {
        //         command: 'node_modules/bower/bin/bower install'
        //     },
        //     server: {
        //         command: 'cd build; php -S localhost:3005'
        //     }
        // },

        clean: {
            build: {
                src: ['build']
            }
        },

        copy: {
            images: {
                files: [{
                    expand: true,
                    cwd: 'src/img/',
                    src: ['**/*', '!**/nonbuild/**'],
                    dest: 'build/img'
                }]
            },
            json: {
                files: [{
                    expand: true,
                    cwd: 'src/',
                    src: ['**/*.json'],
                    dest: 'build'
                }]
            },
            js: {
                files: [{
                    expand: true,
                    cwd: 'src/js',
                    src: ['**/*.js'],
                    dest: 'build/js'
                }]
            },
        },

        sass: {
            options: {
                outputStyle: 'compressed'
            },
            dist: {
                files: {
                    'build/css/content.css' : 'src/scss/content.scss'
                }
            }
        },


        // Scripts
        jshint: {
            options: {
                curly: true,
                eqeqeq: true,
                forin: true,
                freeze: false,
                immed: true,
                indent: 4,
                latedef: true,
                newcap: true,
                noarg: true,
                noempty: true,
                devel: true,
                nonbsp: true,
                nonew: true,
                plusplus: false,
                quotmark: 'single',
                undef: false,
                unused: true,
                strict: false,
                maxparams: 4,
                maxdepth: 4,
                maxlen: 120,
                trailing: true,
                globals: {
                    angular: true,
                    console: false,
                    require: false,
                    data: false,
                    $: false,
                    jQuery: false,
                    google: false
                }
            },
            grunt: {
                options: {
                    node: true
                },
                src: ['Gruntfile.js']
            },
            build: {
                options: {
                    browser: true
                },
                src: [
                'src/js/**/*.js'
                ]
            }
        },
        concurrent: {
            dev: {
                tasks: ['watch'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },

        // 'shell:server',
        // Watch
        watch: {
            sass: {
                files: ['src/scss/**/*.scss'],
                // runs the task `sass` whenever any watched file changes 
                tasks: ['sass']
            },
            js: {
                files: ['src/js/**/*.js'],
                tasks: ['copy:js'],
                options: {
                    spawn: false,
                    livereload: true
                }
            },
            images: {
                files: ['src/img/**/*'],
                tasks: ['copy:images'],
                options: {
                    spawn: false,
                    livereload: true
                }
            },
            json: {
                files: ['src/**/*'],
                tasks: ['copy:json'],
                options: {
                    spawn: false,
                    livereload: true
                }
            }
        }
    });

};