chrome.extension.onRequest.addListener(function(request, sender) {
    if (request === 'show_page_action') {
        chrome.pageAction.show(sender.tab.id);
    }
});

var toggle = true;
chrome.pageAction.onClicked.addListener(function(tab) {
    toggle = !toggle;
    if (toggle) {
        chrome.tabs.sendMessage(tab.id, 'enable');
        chrome.pageAction.setIcon({
            path: 'img/icon48.png',
            tabId: tab.id
        });
        chrome.pageAction.setTitle({
            tabId: tab.id,
            title: 'Disable Asylum Dark Theme'
        });
    } else {
        chrome.tabs.sendMessage(tab.id, 'disable');
        chrome.pageAction.setIcon({
            path: 'img/icon48-off.png',
            tabId: tab.id
        });
        chrome.pageAction.setTitle({
            tabId: tab.id,
            title: 'Enable Asylum Dark Theme'
        });
    }
});