# asylumDark

This is a Google Chrome extension to change the theme of the Asylum forums (http://www.gaming-asylum.com/forums) to a more relaxing dark color. Never get sore eyes staring at the forums again!

Written in JavaScript and Sass. Download the repository and install with node js (npm install).
